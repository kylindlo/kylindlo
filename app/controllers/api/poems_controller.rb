require 'net/http'
require 'net/https'
require 'uri'
require 'json'

class Api::PoemsController < ApplicationController
  skip_before_action :verify_authenticity_token
  def index
    @poems = Poem.all
    render :json => {:status => :ok, :poems => @poems}.to_json
    # render :json => {:status => :ok, :poems => [{
    #     :name => "avb",
    #     :text =>"sddfb",
    #     :id => 1
    #                                             },{
    #     :name => "avb",
    #     :text =>"sddfb",
    #     :id => 2
    # },{
    #     :name => "avb",
    #     :text =>"sddfb",
    #     :id => 3
    # }]}.to_json
  end

  def search
    uri = URI.parse("https://kulyindlo.herokuapp.com/search")
    args = {q: params[:query], platform: 1}
    uri.query = URI.encode_www_form(args)
    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true

    request = Net::HTTP::Get.new(uri.request_uri)

    response = http.request(request)
    data = JSON.parse(response.body)
    render :json => {:status => :ok, :poems => data['poems']}.to_json
  end

  def new
    render "poems/new"
  end

  def create
    params['poem'].permit!
    @poem = Poem.new(params['poem'])
    if @poem.save
      render :json => {:status => :ok, :poem => @poem}
    else
      render :json => {:status => :unprocessable_entity, :errors => @poem.errors}
    end
  end

  def show
    @poem = Poem.find_by(id: params['id'])
    if @poem != nil
      render :json => {:status => :ok, :poem => @poem}
    else
      render :json => {:status => :unprocessable_entity, :errors => @poem.errors}
    end
  end

  def update
    params['poem'].permit!
    @poem = Poem.update(params['id'], params['poem'].except(:id))
    render :json => {:status => :ok, :poem => @poem}.to_json
  end

  def destroy
    @poem = Poem.find_by(id: params['id'])
    Poem.destroy(params['id']) unless @poem == nil
    render :json => {:status => :ok}.to_json
  end
end
