class PoemsController < ApplicationController
  def index
    render "poems/index"
  end

  def new
    render "poems/new"
  end

  def create
    #params['poem']['admin_user_id'] = 1
    params['poem'].permit!
    @poem = Poem.new(params['poem'])
    if @poem.save
      redirect_to "/poems"
    end
  end

  def show
    @poem = Poem.find_by(id: params['id'])
    params[:poem] = {:name => @poem.name,
                    :text => @poem.text,
                    :admin_user_id => @poem.admin_user_id}
    render json: params.to_json
    #render "poems/show"
  end

  def update
    params['poem'].permit!
    @poem = Poem.update(params['id'], params['poem'].except(:id))
    params[:poem] = {:name => @poem.name,
                     :text => @poem.text,
                     :admin_user_id => @poem.admin_user_id}
    redirect_to "/poems"
  end

  def destroy
    @poem = Poem.find_by(id: params['id'])
    Poem.destroy(params['id']) unless @poem == nil
    redirect_to "poems"
  end
end
