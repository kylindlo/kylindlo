Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  # get '/sign', to: 'users#sign'
  # match 'signup', to: 'users#signup', via: [:post]
  # match 'signin', to: 'users#signin', via: [:post]
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  # resources :users, only: [:index]
  resources :poems, only: [:index, :new, :create]# , :show, :update, :destroy]

  # get '/user', to: 'users#show'
  # match 'user', to: 'users#update', via: [:put, :patch]

  namespace :api, defaults: {format: 'json'} do
    match 'poems', to: 'poems#index', via: [:get]
    match 'poems', to: 'poems#search', via: [:post]
  end
end
