class CreatePoems < ActiveRecord::Migration[5.1]
  def change
    create_table :poems do |t|
      t.string :name
      t.text :text
      t.timestamps
      t.belongs_to :admin_user
    end
    create_table :groups do |t|
      t.string :name
      t.integer :level
    end
    create_table :user_data do |t|
      t.integer :rank
      t.text :bio
      t.integer :age
      t.belongs_to :admin_user
    end
    create_table :log do |t|
      t.text :message
      t.date :occ_date
    end
    add_column :admin_users, :group, :integer, null: false;
  end
end