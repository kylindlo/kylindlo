document.addEventListener("turbolinks:load", function(){
    $.ajax({
        url: "/api/poems",
        type: "get",
        dataType: "json",
        success: function(data) {
            var div = document.getElementById('List');
            console.log(data);
            if(div == null)
                return;
            div.innerHTML = "";
            for(var ind in data.poems) {
                div.innerHTML += GeneratePoemForm(data.poems[ind]);
            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            console.log("AJAX error");
            alert("Status: " + textStatus); alert("Error: " + errorThrown);
        }});
});

function GeneratePoemForm(poem){
    return '<div class="col-lg-3 col-md-6 col-sm-6" id=div'+poem.id+'>' +
        '<div class="card card-stats">' +
        '<div class=text-right>' +
        '<button type="button" onclick=deletePoem('+poem.id+') class="btn btn-secondary btn-sm"><i class="material-icons" >clear</i></button>' +
        '</div>' +
        '<div class="card-header card-header-success card-header-icon">' +
        '<br>' +
        '<p class="card-category"> '+poem.name+' </p>' +
        '<hr>' +
        '<h4 class="card-title"> '+ poem.text+' </h4>' +
        '</div>' +
        '</div>' +
        '</div>';
}
function DoSearch(){
    var formdata = {'query': document.getElementById("SearchValue").value};
    console.log(formdata);
    $.ajax({
        'url': "/api/poems",
        'type': "POST",
        'dataType': "json",
        'data': formdata,
        'success': function(data) {
            var div = document.getElementById('List');
            console.log(data);
            if(div == null)
                return;
            div.innerHTML = "";
            for(var ind in data.poems) {
                div.innerHTML += GeneratePoemForm(data.poems[ind]);
            }
        },
        'error': function(XMLHttpRequest, textStatus, errorThrown) {
            console.log("AJAX error");
            alert("Status: " + textStatus); alert("Error: " + errorThrown);
        }});
}

function deletePoem(ind){
    document.getElementById('div'+ind).remove();
    console.log('button'+ind+'clicked');
}


Element.prototype.remove = function() {
    this.parentElement.removeChild(this);
}
NodeList.prototype.remove = HTMLCollection.prototype.remove = function() {
    for(var i = this.length - 1; i >= 0; i--) {
        if(this[i] && this[i].parentElement) {
            this[i].parentElement.removeChild(this[i]);
        }
    }

}
;
